/* 
 * code based on original work by Ph.D Student Ariel Ruiz (ac1753@coventry.ac.uk)
 * adapted into teaching material by Dr Carey Pridgeon (ab0475@coventry.ac.uk)
 */
 /*released under the Apache Public Licence */


#include <iostream>
#include <mpi.h>
#include "MPI_Class.h"

// using namespace std; <- I hate using this

int main(int argc, char** argv) {
  int numProcs, rank, namelen;
  char node_name[MPI_MAX_PROCESSOR_NAME];
  int i;
  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &numProcs);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  //get node name

memset(node_name, 0, MPI_MAX_PROCESSOR_NAME);
  MPI_Get_processor_name(node_name, &namelen);
  memset(node_name+namelen,0,MPI_MAX_PROCESSOR_NAME-namelen);
  Words wordset; 
  if(rank == 0) {
    std::cout << "> Creating data array" << std::endl<< std::flush;
    wordset.createWordSet(18);
}
  if(rank == 0) {
    std::cout << std::endl<< "> This is the original array of  values on " << node_name<<  std::endl;
    wordset.printContent(18);
    std::cout << std::endl<< "> The array is being scattered to the nodes, two array elements per node " <<  std::endl;
  }
  wordset.scatterWords(2);
  if (rank !=0) {
    std::cout << std::endl<< "> Two array elements are now on now on node " << rank << " of " << numProcs <<" - " <<  node_name<<  std::endl;

  }
  
  if (rank ==0) {
    std::cout << std::endl<< "> The word in the array elements is now being changed from 'red' to 'blue' on each node " <<   std::endl;

  }
  // process data on the nodes
  for (i=0;i<numProcs;i++) {
    if (rank == i) {
        wordset.changeVals(2);
    }
  }
  if(rank == 0) {
    std::cout << std::endl<< "> The array data has been processed on the nodes, so it's now being returned to  " << node_name<<  std::endl;
   }
   // send back the data
    wordset.sendWordsFromNode(0,0);
    wordset.sendWordsFromNode(0,1);

  if (rank ==0) {
    std::cout << std::endl<< "> The array elements are  being collected back from the nodes via the COM_WORLD buffer and copied back into the original array" <<  std::endl;
    // retreive the data from the COM_WORLD buffer
  for (i=0;i<numProcs;i++) {
    wordset.receiveWordFromNode(i);
    wordset.receiveWordFromNode(i);  
  }
  std::cout << std::endl<< " > This is the revised array  back on " << node_name<<  std::endl;

  wordset.printContent(18);
  }

  //Best to call here instead of through destructor apparently
   wordset.freeMpiType();                                                   

  MPI_Finalize();
 
  return 0;
}
