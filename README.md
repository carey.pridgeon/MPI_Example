This code is for use in teaching Distributed Computing at Coventry University.

It won't work for anyone else unless the nodeNames file is updated. The size of the data array, 
and the array handling code may also need to be changed.

This code was developed on a Centos Rocks Cluster.