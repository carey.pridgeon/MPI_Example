#include <cstdlib>
#include <time.h>
#include <mpi.h>
#include <vector> 
#include <iostream>  
#include <ostream>
typedef struct {
  char word[5];
  int  id;
}wordStruct;

class Words{
 private: 
  int numWords;
  MPI::Datatype MPI_WORD;  
  wordStruct content[18];
  wordStruct receive[2];
public:
 Words(){
    createWordSet(18);
    createMpiWordType();
    numWords = 18;
 }
  ~Words(){
   // freeMpiType();
  }

  void freeMpiType(){
	MPI_WORD.Free();
  }
  void scattervWords(){
	//If we want to send different number of elements 
	//to the nodes we need to specify the values
        // if we don't and we use scatter instead of scatterv we don't have to, but it's in here to show how it's used
    int arrCounts[9]={2,2,2,2,2,2,2,2,2};
	int displacements[18] = {0};
	int sum(0);
	for(int i(0); i < 18; i++){
	  displacements[i] = sum; // why is the first element set at 0?     
		sum += arrCounts[i];
	}
	MPI::COMM_WORLD.Scatterv(&content, arrCounts, displacements,MPI_WORD, &receive, 2, MPI_WORD, 0);
  }

  void scatterWords(int count) {                                   
    int displacements[18] = {0};
    int sum(0);
    for(int i(0); i < 18; i++){
      displacements[i] = sum; // why is the first element set at 0?
    }
    MPI::COMM_WORLD.Scatter(&content,count,MPI_WORD, &receive,count, MPI_WORD, 0);
  }


  void sendWordsFromNode(int toWho, int index){
	int flag(0);
	wordStruct buf;
	buf = receive[index];
	MPI::COMM_WORLD.Send(&buf,1, MPI_WORD, toWho, flag);
  }
  
  void sendWordsFromMaster(int toWho, int index){
	int flag(0);
	wordStruct buf;
	buf = content[index];
	MPI::COMM_WORLD.Send(&buf,1, MPI_WORD, toWho, flag);
  }
  
  void receiveWordFromNode(int fromWho){
    int flag(0), quantity(1);
    wordStruct buf;
    MPI::COMM_WORLD.Recv(&buf,quantity, MPI_WORD, fromWho, flag);
    content[buf.id] =buf;
  }
  
  
  void receiveWordFromMaster(int fromWho, int index){
    int flag(0), quantity(1);
    wordStruct buf;
    MPI::COMM_WORLD.Recv(&buf,quantity, MPI_WORD, fromWho, flag);
    receive[index] =buf;
  }

  
  
  
  
  void printContent(int num){
    std::cout<< std::endl;

	for(int i(0); i < num; i++){
	  std::cout << "> Array Index:  " << content[i].id<<":  Word: "  <<content[i].word<< std::endl;             	

         }
  }
  void changeVals(int num){
    for(int i(0); i < num; i++){
		 strcpy(receive[i].word,"blue");
    }
  }
  
  void createWordSet(int num){

    for (int i(0); i < num; i++) {
	memset(content[i].word, '\0', sizeof(content[i].word));
        strcpy(content[i].word,"red");
        content[i].id = i;
    }
  }


  void  createMpiWordType(){
    //how many data types in particle struct
    const int count = 2; 
    //type of every block
    MPI::Datatype typesInStruct[count] = {MPI::CHAR,MPI::INT};

    //how many elements per block type ..ie 7 ints..
    //change according to elements in struct 
    int arrayBlockLengths [count] = {5,1};

    //Specify starting memory of each block
    MPI::Aint objAddress, address1,address2;
    MPI::Aint arrayDisplacements[count];
	
    wordStruct sbuf;//Just has to be a struct instance but not
                    // the one you're actually sending
    objAddress = MPI::Get_address(&sbuf);
    address1 = MPI::Get_address(&sbuf.word);
    address2 = MPI::Get_address(&sbuf.id);

    arrayDisplacements[0] = address1 - objAddress;
    arrayDisplacements[1] = address2 - objAddress;

    MPI::Datatype mpiWord;
    mpiWord = MPI::Datatype::Create_struct(count,arrayBlockLengths,arrayDisplacements,typesInStruct);

	mpiWord.Commit();
    this->MPI_WORD = mpiWord;
  }


};
