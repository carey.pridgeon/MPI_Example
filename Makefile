#################################################
# C++ Makefile for 370ct                        #
#################################################

CC = mpic++
CFLAGS = -Wall 
EXE = mpiDemo

OBJS = $(patsubst %.cpp,%.o,$(wildcard *.cpp))

#----------
dining: $(OBJS)
	$(CC) $(LLIBS) $(OBJS)  -o $(EXE)
	rm -f *.o *~

#----------

$(OBJS): %.o : %.cpp
	$(CC) $(CFLAGS) -c $<

#----------

clean:
	rm -f *.o *.*~ *~  $(EXE)

#----------



